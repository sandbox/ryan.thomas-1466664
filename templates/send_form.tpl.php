<div class="send_form_window" style="display: none;">
    <div class="send_form_title">
        <div class="send_from_title_t">Tell a Friend</div>
        <div class="send_form_close_btn"><a href="" onclick="javascript:SendWindow.closeWindow(this);  return false;">X</a></div>
    </div>
    <div class="send_form_body">
        <p class="desc">To send to multiple recipients, separate each address by a comma.</p>
        <form action="" method="post" name="sendForm">
            <div>
                    <div style="float: left;  width: 49%; ">
                            <label for="rec_email">Recipient's Email</label><label class="rec_email_errors send_errors" style="color: red;"></label>
                            <br />
                            <input name="rec_email" class="send_form_recemail" type="text" />
                            <br />

                            <label for="your_name">Your Name</label><label class="name_errors send_errors" style="color: red;"></label>
                            <br />
                            <input name="your_name" class="send_form_yname" type="text" value="<?php  global $user; print $user->name; ?>" />
                            <br />

                            <label for="your_email">Your Email</label><label class="yemail_errors  send_errors" style="color: red;"></label>
                            <br />
                            <input name="your_email" class="send_form_yemail" type="text" />
                            <br />
                   </div>
                    <div style="float: left; width: 49%;">
                            <label for="message">Message (optional)</label>
                            <textarea name="message" class="send_form_message" rows="4" ></textarea>
                            <label class="message_errors send_errors" style="color: red;"></label>
                            <br />
                    </div>
                    <div class="send_form_answer" style="padding-top: 10px; width: 100%; clear:both; font-size: 14px; "></div>
                    <div style="width: 100%; clear:both;">
                            <input name="send" class="send_form_btn" type="button" value="Send" onclick="SendWindow.sendRequest();" />
                    </div>
            </div>
        </form>
        <p class="desc">We don't spam. The information that you enter will be used for this communication only.</p>
    </div>
</div>