    if(Drupal.jsEnabled) {

            SendWindow = {}

            SendWindow.indx = 0;

            SendWindow.openWindow = function() {

                var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
                var dsoctop = document.all ? iebody.scrollTop : pageYOffset;

                $('.send_form_window').css({ 'top': (dsoctop + 300), 'left':(window.screen.width / 2 - 225)  });
                $('.send_form_window').css('display','block');


                return false;
            }

            SendWindow.closeWindow = function() {

               // $('.send_form_window').css({ 'top': (dsoctop + 300), 'left':(window.screen.width / 2 - 150)  });
                $('.send_form_window').css('display','none');

               //document.forms.sendForm.your_name.value = "";
               document.forms.sendForm.your_email.value = "";
               document.forms.sendForm.rec_email.value = "";
               document.forms.sendForm.message.value = "";
                $(".send_errors").text("");
                return false;
            }

            SendWindow.sendRequest = function() {

                    // sendForm
                    if(!SendWindow.validate()) return false;


                    var name       = document.forms.sendForm.your_name.value;
                    var yemail      =  document.forms.sendForm.your_email.value;
                    var rec_email =  document.forms.sendForm.rec_email.value;
                    var message  =  document.forms.sendForm.message.value;
                    var link          =  window.location.toString();
                    

                    var url = '/sendtofriends/send';

                    
                 /*   alert(window.location);*/

                     var parametrs    = {name: name, yemail: yemail, rec_email: rec_email, message: message, link: link };
                    $.ajax({
                        async: false,
                        cache: false,
                        url: url,
                        type: "post",
                        data: parametrs,
                        success: function(data) {


                           var answer = data.documentElement.childNodes[0].nodeValue;

                           if(answer == "true") {

                               var message = "Your message was sent! Thank you!";

                           }else {

                                var message = "Your message was not sent! Try again later!";

                           }

                           $(".send_form_answer").fadeOut();
                           $(".send_form_answer").text(message);
                           $(".send_form_answer").fadeIn();

                        }
                    });
                

            }

            SendWindow.rollWindow = function() {

                        var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
                        var dsoctop = document.all ? iebody.scrollTop : pageYOffset;

                        $(".send_form_window").animate({

                            'top': dsoctop + 300

                        }, 1500);

            }

            SendWindow.validate = function() {
                    var flag_name = true;
                    var flag_message = true;
                    var flag_yemail=true;
                    var flag_rec_email = true;

                    var name       = document.forms.sendForm.your_name.value;
                    var yemail      =  document.forms.sendForm.your_email.value;
                    var rec_email =  document.forms.sendForm.rec_email.value;
                    var message  =  document.forms.sendForm.message.value;

                    var name_message = "";
                    var message_message = "";
                    var yemail_message = "";
                    var rec_email_message = "";

                    if(name == "") {
                        
                        name_message = "*Field can not be empty!";
                        flag_name = false;
                        $(".name_errors").text(name_message);
                        
                    }else {

                        $(".name_errors").text("");
                        
                    }

                    if(message == "") {

                        message_message = "*Field can not be empty!";
                        flag_message = false;
                        $(".message_errors").text(message_message);

                    }else {

                        $(".message_errors").text("");

                    }

                    if(yemail == "") {

                        yemail_message = "*Field can not be empty!";
                         $(".yemail_errors").text(yemail_message);
                         flag_yemail = false;

                    }else {

                               var pattern   = /^[-a-zA-Z\.\_0-9]+@[a-zA-Z]+\.[a-z]{2,3}$/i;
                               var res       = pattern.exec(yemail);
                               
                               if(res) {

                                   yemail_message = "";
                                   $(".yemail_errors").text(yemail_message);
                                   
                               }else {

                                   yemail_message = "*Invalid email";
                                   $(".yemail_errors").text(yemail_message);
                                   flag_yemail = false;
                                   
                               }
                               

                    }

                    if(rec_email == "") {

                        rec_email_message = "*Field can not be empty!";
                        $(".rec_email_errors").text(rec_email_message);
                        rec_email_message = false;

                        
                    }else {

                        var emails = (new String(rec_email)).split(/\s*,\s*/);
                        
                        if(emails.length > 1) {

                                       var pattern1   = /^[-a-zA-Z\.\_0-9]+@[a-zA-Z]+\.[a-z]{2,3}$/i;
                                       var res1       = null;
                            
                            for(i = 0; i < emails.length; ++i) {

                                       res1       = pattern1.exec(emails[i]);

                                       if(res1) {

                                           rec_email_message = "";
                                           $(".rec_email_errors").text(rec_email_message);

                                       }else {

                                           rec_email_message = "*Invalid email";
                                           $(".rec_email_errors").text(rec_email_message);
                                           rec_email_message = false;
                                           break;

                                       }
                                       
                            }

                        }else {

                                       var pattern2   = /^[-a-zA-Z\.\_0-9]+@[a-zA-Z]+\.[a-z]{2,3}$/i;
                                       var res2       = pattern2.exec(rec_email);

                                       if(res2) {

                                           rec_email_message = "";
                                           $(".rec_email_errors").text(rec_email_message);

                                       }else {

                                           rec_email_message = "*Invalid email";
                                           $(".rec_email_errors").text(rec_email_message);
                                           rec_email_message = false;

                                       }

                                }

                        

                    }

                    if(flag_name && flag_message && flag_yemail && flag_rec_email) {

                        return true;

                    }esle
                        return false;

            }

            function _rollWindow() {
                
                        var iebody = (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
                        var dsoctop = document.all ? iebody.scrollTop : pageYOffset;
                
                        $(".send_form_window").animate({

                            'top': dsoctop + 300

                        }, 300);

            }

            $(document).ready(function() {

                $(window).scroll(function() {

                        clearTimeout(SendWindow.indx);
                        SendWindow.indx =  setTimeout("_rollWindow()", 500);

                });

            });

    }
